package us.edwardstx.rpn;

final class RPNObjectFunction {
	private final Class<? extends RPNObject> clazz;
	private final String cmd;

	public RPNObjectFunction(Class<? extends RPNObject> clazz, String cmd) {
		this.clazz = clazz;
		this.cmd = cmd;
	}

	public Class<? extends RPNObject> getClazz() {
		return clazz;
	}

	public String getCmd() {
		return cmd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
		result = prime * result + ((cmd == null) ? 0 : cmd.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RPNObjectFunction other = (RPNObjectFunction) obj;
		if (clazz == null) {
			if (other.clazz != null)
				return false;
		} else if (!clazz.equals(other.clazz))
			return false;
		if (cmd == null) {
			if (other.cmd != null)
				return false;
		} else if (!cmd.equals(other.cmd))
			return false;
		return true;
	}

}
