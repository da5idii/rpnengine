package us.edwardstx.rpn.exception;

public abstract class RPNException extends Exception {
	private static final long serialVersionUID = 1L;
	public RPNException(String m, Throwable e) {
		super(m, e);
	}
	public RPNException(String m) {
		super(m);
	}

}
