package us.edwardstx.rpn.exception;

public class UnknowFunctionException extends RPNException {
	private static final long serialVersionUID = 5044101300231585546L;

	public UnknowFunctionException(String m) {
		super(m);
	}
}
