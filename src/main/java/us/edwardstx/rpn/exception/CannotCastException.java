package us.edwardstx.rpn.exception;

public class CannotCastException extends RPNException {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("rawtypes")
	public CannotCastException(Class c1, Class c2) {
		super("Cannot cast " + c1 + " to " + c2);
			// TODO Auto-generated constructor stub
	}
}
