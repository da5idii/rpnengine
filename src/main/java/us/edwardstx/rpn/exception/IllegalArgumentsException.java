package us.edwardstx.rpn.exception;

public class IllegalArgumentsException extends RPNException {
	private static final long serialVersionUID = 2740484077238472854L;
	public IllegalArgumentsException() {
		super("Illegal Arguments");
	}
}
