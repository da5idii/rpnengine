package us.edwardstx.rpn.exception;

public class InvocationException extends RPNException {
	private static final long serialVersionUID = 8449141183668687965L;

	public InvocationException(String cmd, Throwable e) {
		super("Invocation Exception " + cmd, e);
	}
}
