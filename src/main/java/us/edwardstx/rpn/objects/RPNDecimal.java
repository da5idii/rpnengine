package us.edwardstx.rpn.objects;

import java.math.BigDecimal;

import org.apache.commons.math3.util.BigReal;

import us.edwardstx.rpn.RPNObject;
import us.edwardstx.rpn.RPNParsable;
import us.edwardstx.rpn.RPNStackFunction;

public class RPNDecimal implements RPNValue<BigReal>{
	private static final long serialVersionUID = -2875009974445479732L;
	private final BigReal value;
	
	public RPNDecimal(Double v) {
		value = new BigReal(v);
	}
	
	public RPNDecimal(RPNInteger v) {
		value = new BigReal(v.getValue());
	}

	public RPNDecimal(BigDecimal v) {
		value = new BigReal(v);
	}
	
	public RPNDecimal(BigReal v) {
		value = v;
	}

	@RPNParsable("\\A[+,-]?[0-9]*\\.[0-9]+\\Z")
	public RPNDecimal(String s) {
		value = new BigReal(s);
	}

	@Override
	public BigReal getValue() {
		return value;
	}

	@Override
	public Class<? extends BigReal> getValueClass() {
		return BigReal.class;
	}

	
	@RPNStackFunction(value="+",reciprocal=true)
	public RPNObject add(RPNDecimal rhs) {
		return new RPNDecimal(value.add(rhs.value));
	}
	
	@Override
	public String toString() {
		return "D" + value.bigDecimalValue().toString();
	}
}
