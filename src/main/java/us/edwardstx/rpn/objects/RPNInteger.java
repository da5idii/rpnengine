package us.edwardstx.rpn.objects;

import java.math.BigInteger;

import us.edwardstx.rpn.RPNObject;
import us.edwardstx.rpn.RPNParsable;
import us.edwardstx.rpn.RPNStackFunction;
import us.edwardstx.rpn.exception.CannotCastException;

public class RPNInteger implements RPNValue<BigInteger> {
	private static final long serialVersionUID = -8011603571136251601L;
	private final BigInteger value;

	public RPNInteger(Integer v) {
		value = BigInteger.valueOf(v);
	}

	public RPNInteger(BigInteger v) {
		value = v;
	}

	@RPNParsable("\\A[+,-]?[0-9]+\\Z")
	public RPNInteger(String s) {
		value = new BigInteger(s);
	}

	@Override
	public BigInteger getValue() {
		return value;
	}

	@Override
	public Class<BigInteger> getValueClass() {
		// TODO Auto-generated method stub
		return BigInteger.class;
	}

	@RPNStackFunction(value="+",reciprocal=true)
	public RPNObject add(RPNInteger rhs) {
		return new RPNInteger(value.add(rhs.value));
	}
	
	@Override
	public String toString() {
		return "I" + value.toString();
	}


	@Override
	public RPNObject assignTo(Class<? extends RPNObject> clazz)
			throws CannotCastException {
		return RPNValue.super.assignTo(clazz);
	}

}
