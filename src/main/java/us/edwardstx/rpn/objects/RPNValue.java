package us.edwardstx.rpn.objects;

import java.io.Serializable;

import us.edwardstx.rpn.RPNObject;

public interface RPNValue<T extends Serializable> extends RPNObject ,Serializable {

	T getValue();
	Class<? extends T> getValueClass();
}
