package us.edwardstx.rpn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Set;

import org.reflections.Reflections;

import us.edwardstx.rpn.exception.RPNException;

public class RPNEngineImpl {
	private final RPNStack stack = new RPNStack();
	private final Executer executer = new Executer();
	private final Parser parser = new Parser();
	
	public RPNEngineImpl() {
		Reflections reflections = new Reflections("us.edwardstx.rpn");
		Set<Class<? extends RPNObject>> types = reflections.getSubTypesOf(RPNObject.class);
		for (Class<? extends RPNObject> clazz : types) {
			System.out.println(clazz);
			parser.addParsable(clazz);
			executer.addFunctions(clazz);
		}
	}
	
	public void printStack(PrintStream out) {
		if (stack.depth() == 0)
			out.println("(EMPTY)");
		else
			for (int i = stack.depth() - 1; i >= 0; i--) {
				out.println(i + ": " + stack.get(i).toString());
			}
	}

	public void execute(String s) {
		RPNObject obj = parser.parse(s);
		if (obj != null) {
			stack.push(obj);
			return;
		}
		
		try{
			executer.execute(s, stack);
			return;
		} catch (RPNException e){
			System.out.print("%" + s + "\t");
			e.printStackTrace();
		}
	}

	public String getItem(Integer level) {
		return stack.get(level).toString();
	}

	public static void main(String[] args) {
		RPNEngineImpl engine = new RPNEngineImpl();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			engine.printStack(System.out);
			System.out.print(">");
			try {
				String input = in.readLine();
				if(input.trim().length() >= 0)
					engine.execute(input);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}


}
