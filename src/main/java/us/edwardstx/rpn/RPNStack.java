package us.edwardstx.rpn;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

public class RPNStack implements RPNComposite {
	private static final long serialVersionUID = 7581572783064841096L;
	private Integer depth = 0;
	private LinkedList<RPNObject> heap = new LinkedList<RPNObject>();
	Map<String, Integer> variableMap = new HashMap<String, Integer>();

	@Override
	public boolean containsKey(String key) {
		return variableMap.containsKey(key);
	}

	public RPNObject get(Integer index) {
		index = Objects.requireNonNull(index);
		if (index < 0)
			throw new IllegalArgumentException();
		if (index >= depth)
			throw new IndexOutOfBoundsException();
		return heap.get(index);
	}

	@Override
	public RPNObject get(String key) {
		Integer index = stackVar(key);
		if (index != null) {
			if (index >= depth)
				throw new IndexOutOfBoundsException();
			return heap.get(index);
		}
		index = variableMap.get(key);
		if (index != null) {
			index += depth;
			return heap.get(index);
		}
		return null;
	}

	@Override
	public RPNObject put(String key, RPNObject value) {
		Integer index = stackVar(key);
		RPNObject ret = null;
		if (index != null) {
			if (index >= depth)
				throw new IndexOutOfBoundsException();
			ret = heap.get(index);
			heap.set(index, value);
		} else if (variableMap.containsKey(key)) {
			index = variableMap.get(key);
			ret = heap.get(index);
			heap.set(index + depth, value);
		} else {
			index = heap.size() - depth;
			heap.addLast(value);
			variableMap.put(key, index);
		}
		return ret;
	}

	@Override
	public Set<String> keySet() {
		return variableMap.keySet();
	}

	@Override
	public int depth() {
		return depth;
	}

	private static Integer stackVar(String name) {
		if (name == null || name.length() == 0)
			throw new IllegalArgumentException("Varible name must be non-null and non-empty");
		if (!name.startsWith(STACK_VAR_PREFIX) || name.length() == STACK_VAR_PREFIX.length())
			return null;
		String val = name.substring(STACK_VAR_PREFIX.length());
		try {
			return Integer.parseInt(val);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	@Override
	public void push(RPNObject obj) {
		heap.addFirst(obj);
		depth++;
	}

	@Override
	public RPNObject pop() {
		if (depth == 0)
			throw new NoSuchElementException();
		depth--;
		return heap.pop();
	}

	@Override
	public RPNObject peek() {
		return heap.peek();
	}

	@Override
	public int size() {
		return heap.size();
	}

	@Override
	public boolean isEmpty() {
		return heap.isEmpty();
	}

	@Override
	public int clearStack() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int clearVars() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int clearAll() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public RPNObject delete(String key) {
		Integer index = stackVar(key);
		RPNObject ret = null;
		if (index != null) {
			if (index >= depth)
				throw new IndexOutOfBoundsException();
			ret = heap.get(index);
			int i = index;
			heap.remove(i);
			depth--;
		} else if (variableMap.containsKey(key)) {
			final Integer keyIndex = variableMap.get(key);
			ret = heap.get(keyIndex + depth);
			heap.remove(keyIndex + depth);
			variableMap.remove(key);
			variableMap.replaceAll((x, y) -> {
				if (y >= keyIndex)
					return y - 1;
				else
					return y;
			});
		}
		return ret;
	}

	@Override
	public Iterator<RPNObject> iterator() {
		return heap.iterator();
	}

	@RPNStackFunction("DUP")
	public static void dup(RPNStack stack) {
		stack.push(stack.peek());
	}

	@Override
	public void drop() {
		this.pop();

	}

	@Override
	public void drop(Integer i) {
		for (int cnt = 0; cnt < i; cnt++)
			this.drop();
	}

}
