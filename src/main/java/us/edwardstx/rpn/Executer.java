package us.edwardstx.rpn;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import us.edwardstx.rpn.exception.CannotCastException;
import us.edwardstx.rpn.exception.IllegalArgumentsException;
import us.edwardstx.rpn.exception.InvocationException;
import us.edwardstx.rpn.exception.RPNException;
import us.edwardstx.rpn.exception.UnknowFunctionException;

public class Executer {

	private final Map<String, Method> stackFunctions = new HashMap<String, Method>();
	private final Map<RPNObjectFunction, Set<Method>> objectMethods = new HashMap<RPNObjectFunction, Set<Method>>();

	public void addFunctions(Class<? extends RPNObject> clazz) {
		for (Method method : clazz.getMethods()) {
			RPNStackFunction stackFunction = method.getAnnotation(RPNStackFunction.class);
			if (stackFunction == null)
				continue;
			if (Modifier.isStatic(method.getModifiers())) {
				if (!method.getReturnType().equals(Void.TYPE))
					continue;
				if (method.getParameterCount() != 1)
					continue;
				if (!method.getParameters()[0].getType().equals(RPNStack.class))
					continue;
				System.out.println("Adding stack method " + method);
				for (String name : stackFunction.value())
					stackFunctions.put(name, method);
			} else {
				if (!RPNObject.class.isAssignableFrom(method.getReturnType())
						&& Collection.class.isAssignableFrom(method.getReturnType()))
					continue;
				else {
					if (method.getParameterCount() > 0)
						for (Parameter parameter : method.getParameters()) {
							if (!RPNObject.class.isAssignableFrom(parameter.getType()))
								continue;
						}
					for (String name : stackFunction.value()) {
						RPNObjectFunction key = new RPNObjectFunction(clazz, name);
						Set<Method> methods;
						if (objectMethods.containsKey(key))
							methods = objectMethods.get(key);
						else
							objectMethods.put(key, methods = new HashSet<>());
						methods.add(method);
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void execute(String cmd, RPNObject obj, RPNStack stack) throws RPNException {
		RPNException excpetion = new UnknowFunctionException(cmd);
		RPNObjectFunction key = new RPNObjectFunction(obj.getClass(), cmd);
		if (objectMethods.containsKey(key)) {
			excpetion = new IllegalArgumentsException();
			for (Method method : objectMethods.get(key)) {
				try {
					int cnt = method.getParameterCount();
					if (cnt >= stack.depth())
						continue;
					Object paramaters[] = new Object[cnt];
					for (int i = 0; i < cnt; i++) {
						Class<? extends RPNObject> clazz = (Class<? extends RPNObject>) method.getParameters()[i].getType();
						Object paramater = stack.get(i + 1).assignTo(clazz);
						paramaters[i] = paramater;
					}
					if (RPNObject.class.isAssignableFrom(method.getReturnType())) {
						RPNObject ret = (RPNObject) method.invoke(obj, paramaters);
						stack.drop(1 + cnt);
						stack.push(ret);
						return;
					} else {
						Collection<RPNObject> ret = (Collection<RPNObject>) method.invoke(obj, paramaters);
						stack.drop(1 + cnt);
						for (RPNObject o : ret) {
							stack.push(o);
						}
						return;
					}
				} catch (Exception e) {
					throw convertToRPNException(cmd,e);
				}
			}
		}
		throw excpetion;
	}

	public void execute(String cmd, RPNStack stack) throws RPNException {
		RPNException excpetion = new UnknowFunctionException(cmd);
		if (stack.depth() >= 1) {
			RPNObject obj = stack.peek();
			try {
				execute(cmd, obj, stack);
				return;
			} catch (Exception e) {
				excpetion = convertToRPNException(cmd,e);
			}
			if (stack.depth() >= 2) {
				obj = stack.peek();
				RPNObject obj2 = stack.get(1);
				try {
					obj = reciprocal(cmd, obj2.getClass(), obj);
					execute(cmd, obj, stack);
					return;
				} catch (Exception e) {
					excpetion = convertToRPNException(cmd,e);
				}
			}
		}
		if (stackFunctions.containsKey(cmd)) {
			Method method = stackFunctions.get(cmd);
			try {
				method.invoke(null, stack);
				return;
			} catch (Exception e) {
				excpetion = convertToRPNException(cmd,e);
			}
		}
		throw excpetion;
	}

	public RPNObject reciprocal(String cmd, Class<? extends RPNObject> clazz, RPNObject obj1) throws CannotCastException {
		RPNObjectFunction key = new RPNObjectFunction(clazz, cmd);
		if (objectMethods.containsKey(key)) {
			for (Method method : objectMethods.get(key)) {
				RPNStackFunction stackFunction = method.getAnnotation(RPNStackFunction.class);
				if (stackFunction.reciprocal() && method.getParameterCount() == 1) {
					try {
						Constructor<? extends RPNObject> constructor = clazz.getConstructor(obj1.getClass());
						return constructor.newInstance(obj1);
					} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
							| IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		}
		throw new CannotCastException(obj1.getClass(), clazz);
	}

	private static RPNException convertToRPNException(String cmd, Exception e) {
		if (e instanceof RPNException)
			return (RPNException) e;
		else {
			e.printStackTrace();
			return new InvocationException(cmd,e);
		}
	}
}
