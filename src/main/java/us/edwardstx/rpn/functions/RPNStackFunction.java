package us.edwardstx.rpn.functions;

import java.util.function.Consumer;

import us.edwardstx.rpn.RPNStack;

@FunctionalInterface
public interface RPNStackFunction extends Consumer<RPNStack>{

}
