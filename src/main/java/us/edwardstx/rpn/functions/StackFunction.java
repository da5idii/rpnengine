package us.edwardstx.rpn.functions;

import java.util.function.Consumer;

import us.edwardstx.rpn.RPNStack;

@FunctionalInterface
public interface StackFunction extends Consumer<RPNStack>{

	default Integer minDeapth(){ return 0;}
}
