package us.edwardstx.rpn;

import java.io.Serializable;
import java.util.Set;

public interface RPNComposite extends Iterable<RPNObject>, Serializable, RPNObject {
	public static final String STACK_VAR_PREFIX = "$";
	boolean containsKey(String key);
	RPNObject get(String key);
	void push(RPNObject obj);
	RPNObject pop();
	RPNObject peek();
	RPNObject put(String key, RPNObject value);
	void drop();
	void drop(Integer i);
	
	RPNObject delete(String key);
	Set<String> keySet();
	int depth();
	int size();
	int clearStack();
	int clearVars();
	int clearAll();
	boolean isEmpty();
	
}
