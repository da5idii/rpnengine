package us.edwardstx.rpn;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


public class Parser {
	Set<Parsable> parsables = new HashSet<>();
	
	
	public RPNObject parse(String s){
		RPNObject obj = null;
		for (Parsable parsable : parsables) {
			obj = parsable.parse(s);
			if (obj != null)
				return obj;
		}
		return null;
	}
	
	public void addParsable(Class<? extends RPNObject> clazz) {
		try {
			Constructor<? extends RPNObject> constructor = clazz.getConstructor(String.class);
			RPNParsable parsable = constructor.getAnnotation(RPNParsable.class);
			if (parsable != null) {
				System.out.println("\tC: " + constructor + " with " + parsable.value());
				parsables.add(new Parsable(constructor));
			}
		} catch (NoSuchMethodException | PatternSyntaxException e) {

		}
	}
	
	
	private static class Parsable {
		private final Pattern pattern;
		private final Constructor<? extends RPNObject> constructor;

		
		public Parsable(Constructor<? extends RPNObject> constructor) {
			this.constructor = Objects.requireNonNull(constructor);
			RPNParsable parsable = constructor.getAnnotation(RPNParsable.class);
			String stringPattern = parsable.value();
			if (!stringPattern.startsWith("\\A"))
				stringPattern = "\\A" + stringPattern;
			if (!stringPattern.endsWith("\\Z"))
				stringPattern = stringPattern + "\\Z";
			this.pattern = Pattern.compile(parsable.value());
		}

		public RPNObject parse(String s) {
			Matcher matcher = pattern.matcher(s);
			if (matcher.find())
				try {
					return constructor.newInstance(s.trim());
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return null;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((constructor == null) ? 0 : constructor.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Parsable other = (Parsable) obj;
			if (constructor == null) {
				if (other.constructor != null)
					return false;
			} else if (!constructor.equals(other.constructor))
				return false;
			return true;
		}

	}
}
