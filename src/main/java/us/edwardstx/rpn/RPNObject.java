package us.edwardstx.rpn;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import us.edwardstx.rpn.exception.CannotCastException;

public interface RPNObject {
	default RPNObject assignTo(Class<? extends RPNObject> clazz) throws CannotCastException {
		if (clazz.isInstance(this)) {
			return this;
		}
		try {
			Constructor<? extends RPNObject> constructor = clazz.getConstructor(this.getClass());
			return constructor.newInstance(this);
		} catch (NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new CannotCastException(this.getClass(), clazz);
		}
	}
}
