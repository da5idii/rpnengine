package us.edwardstx.rpn;

import static org.junit.Assert.*;

import org.junit.Test;

public class RPNTestObjectTest {
	
	private static final RPNTestObject t1 = new RPNTestObject("t1");
	private static final RPNTestObject t2 = new RPNTestObject("t1");
	private static final RPNTestObject t3 = new RPNTestObject("t3");
	
	@Test(expected=NullPointerException.class)
	public void nullException(){
		RPNTestObject t =new RPNTestObject(null);
		assertNull(t);
	}
	
	@Test
	public void objectEqualsTest(){
		assertEquals(t1, t2);
		assertEquals(t1.compareTo(t2), 0);
		assertNotEquals(t1, t3);
		assertNotEquals(t1.compareTo(t3), 0);
	}
	
	@Test
	public void stringEqualsTest(){
		assertEquals(t1.getString(), "t1");
	}
	
}
