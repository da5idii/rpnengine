package us.edwardstx.rpn;

import java.util.Objects;

import us.edwardstx.rpn.exception.CannotCastException;

public class RPNTestObject implements Comparable<RPNTestObject>,
		RPNObject {
	private static final String PARSER_PATTERN = "\\\".*\\\"";
	private final String string;

	@RPNParsable(PARSER_PATTERN)
	public RPNTestObject(String s) {
		string = Objects.requireNonNull(s);
	}

	public String getString() {
		return string;
	}

	@Override
	public int compareTo(RPNTestObject o) {
		return string.compareTo(o.string);
	}

	@Override
	public int hashCode() {
		return string.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RPNTestObject other = (RPNTestObject) obj;
		if (string == null) {
			if (other.string != null)
				return false;
		} else if (!string.equals(other.string))
			return false;
		return true;
	}

	public String toString() {
		return "RPNTestObject: " + string;
	}


	public RPNObject assignTo(Class<? extends RPNObject> clazz)
			throws CannotCastException {
		return RPNObject.super.assignTo(clazz);
	};
	
	@RPNStackFunction("DDUP")
	public static void dup(RPNStack stack){
		stack.push(stack.peek());
		stack.push(stack.peek());
	}

}
