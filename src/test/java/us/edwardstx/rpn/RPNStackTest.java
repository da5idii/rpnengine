package us.edwardstx.rpn;

import static org.junit.Assert.*;

import org.junit.Test;

public class RPNStackTest {
	private static final RPNTestObject s1 = new RPNTestObject("s1");
	private static final RPNTestObject s2 = new RPNTestObject("s2");
	private static final RPNTestObject s3 = new RPNTestObject("s3");
	private static final RPNTestObject s4 = new RPNTestObject("s4");
//	private static final RPNTestObject s5 = new RPNTestObject("s5");
	
	private static final String n1 = "n1";
	private static final RPNTestObject v1 = new RPNTestObject("v1");
	private static final String n2 = "n2";
	private static final RPNTestObject v2 = new RPNTestObject("v2");
	private static final String n3 = "n3";
	private static final RPNTestObject v3 = new RPNTestObject("v3");
	private static final String n4 = "n4";
	private static final RPNTestObject v4 = new RPNTestObject("v4");
//	private static final String n5 = "n5";
	private static final RPNTestObject v5 = new RPNTestObject("v5");
	
	
	@Test(expected=IllegalArgumentException.class)
	public void deleteNullIllegalArgumentException(){
		RPNStack stack = new RPNStack();
		stack.delete(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void deleteEmptyIllegalArgumentException(){
		RPNStack stack = new RPNStack();
		stack.delete("");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getEmptyIllegalArgumentException(){
		RPNStack stack = new RPNStack();
		stack.get("");
	}
	
	@Test(expected=NullPointerException.class)
	public void getNullIllegalArgumentException(){
		RPNStack stack = new RPNStack();
		Integer i = null;
		stack.get(i);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void putEmptyIllegalArgumentException(){
		RPNStack stack = new RPNStack();
		stack.put("",s1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void putNullIllegalArgumentException(){
		RPNStack stack = new RPNStack();
		stack.put(null,s1);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void deleteIndexOutOfBoundsException(){
		RPNStack stack = new RPNStack();
		RPNObject t;
		
		stack.push(s1);
		stack.push(s2);
		
		t = stack.delete(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s2, t);
		
		t = stack.delete(RPNComposite.STACK_VAR_PREFIX + 1);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void getIndexOutOfBoundsException(){
		
		RPNStack stack = new RPNStack();
		RPNObject t;
		
		stack.push(s1);
		stack.push(s2);
		
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s2, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 1);
		assertEquals(s1, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 2);
		
	}
	
	@Test
	public void stackVaribleDelete(){
		RPNStack stack = new RPNStack();
		RPNObject t;
		
		stack.push(s1);
		stack.push(s2);
		stack.push(s3);
		stack.push(s4);
		
		
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s4, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 1);
		assertEquals(s3, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 2);
		assertEquals(s2, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 3);
		assertEquals(s1, t);
		
		assertEquals(4,stack.size());
		assertEquals(4,stack.depth());
		
		t = stack.delete(RPNComposite.STACK_VAR_PREFIX + 2);
		assertEquals(s2, t);
		
		assertEquals(3,stack.depth());
		assertEquals(3,stack.size());

		
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s4, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 1);
		assertEquals(s3, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 2);
		assertEquals(s1, t);
		
		t = stack.delete(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s4, t);
		
		assertEquals(2,stack.size());
		assertEquals(2,stack.depth());
		
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s3, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 1);
		assertEquals(s1, t);
		
		t = stack.delete(RPNComposite.STACK_VAR_PREFIX + 1);
		assertEquals(s1, t);
		
		assertEquals(1,stack.size());
		assertEquals(1,stack.depth());
		
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s3, t);
		
		t = stack.delete(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s3, t);
		
		assertEquals(0,stack.size());
		assertEquals(0,stack.depth());
		
		
	}
	
	@Test
	public void stackVarible(){
		RPNStack stack = new RPNStack();
		RPNObject t;
		
		stack.push(s1);
		t = stack.peek();
		assertEquals(s1, t);
		
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s1, t);
		
		stack.push(s2);
		t = stack.peek();
		assertEquals(s2, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s2, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 1);
		assertEquals(s1, t);
		
		stack.put(n1, v1);
		t = stack.peek();
		assertEquals(s2, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s2, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 1);
		assertEquals(s1, t);
		
		
		stack.push(s3);
		t = stack.peek();
		assertEquals(s3, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 0);
		assertEquals(s3, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 1);
		assertEquals(s2, t);
		t = stack.get(RPNComposite.STACK_VAR_PREFIX + 2);
		assertEquals(s1, t);
		
	}
	
	@Test
	public void basicVar(){
		
		RPNStack stack = new RPNStack();
		RPNObject t;
		
		assertEquals(0,stack.size());
		assertEquals(0,stack.depth());
		
		stack.push(s1);
		assertEquals(1,stack.depth());
		assertEquals(1,stack.size());
		
		stack.put(n1, v1);
		assertEquals(1,stack.depth());
		assertEquals(2,stack.size());
		t = stack.peek();
		assertEquals(s1, t);
		
		t = stack.get(n1);
		assertEquals(v1, t);
		
		stack.push(s2);
		t = stack.get(n1);
		assertEquals(v1, t);
		assertEquals(2,stack.depth());
		assertEquals(3,stack.size());
		
		stack.put(n2, v2);
		assertEquals(2,stack.depth());
		assertEquals(4,stack.size());
		
		t = stack.get(n1);
		assertEquals(v1, t);
		t = stack.get(n2);
		assertEquals(v2, t);
		
		stack.push(s3);
		assertEquals(3,stack.depth());
		assertEquals(5,stack.size());
		t = stack.get(n1);
		assertEquals(v1, t);
		t = stack.get(n2);
		assertEquals(v2, t);
		
		stack.put(n3, v3);
		assertEquals(3,stack.depth());
		assertEquals(6,stack.size());
		
		t = stack.get(n1);
		assertEquals(v1, t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertEquals(v3, t);
		
		stack.push(s4);
		assertEquals(4,stack.depth());
		assertEquals(7,stack.size());
		t = stack.get(n1);
		assertEquals(v1, t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertEquals(v3, t);
		
		t = stack.pop();
		assertEquals(s4, t);
		assertEquals(3,stack.depth());
		assertEquals(6,stack.size());
		t = stack.get(n1);
		assertEquals(v1, t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertEquals(v3, t);
		
		stack.put(n4, v4);
		assertEquals(3,stack.depth());
		assertEquals(7,stack.size());
		t = stack.get(n1);
		assertEquals(v1, t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertEquals(v3, t);
		t = stack.get(n4);
		assertEquals(v4, t);
		
		stack.put(n1, v5);
		assertEquals(3,stack.depth());
		assertEquals(7,stack.size());
		t = stack.get(n1);
		assertEquals(v5, t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertEquals(v3, t);
		t = stack.get(n4);
		assertEquals(v4, t);
		
		t = stack.delete(n3);
		assertEquals(v3, t);
		assertEquals(3,stack.depth());
		assertEquals(6,stack.size());
		t = stack.get(n1);
		assertEquals(v5, t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertNull(t);
		t = stack.get(n4);
		assertEquals(v4, t);
		
		t = stack.delete(n3);
		assertNull(t);
		assertEquals(3,stack.depth());
		assertEquals(6,stack.size());
		t = stack.get(n1);
		assertEquals(v5, t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertNull(t);
		t = stack.get(n4);
		assertEquals(v4, t);
		
		t = stack.delete(n1);
		assertEquals(v5, t);
		assertEquals(3,stack.depth());
		assertEquals(5,stack.size());
		t = stack.get(n1);
		assertNull(t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertNull(t);
		t = stack.get(n4);
		assertEquals(v4, t);
		
		
		t = stack.pop();
		assertEquals(s3, t);
		assertEquals(2,stack.depth());
		assertEquals(4,stack.size());
		t = stack.get(n1);
		assertNull(t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertNull(t);
		t = stack.get(n4);
		assertEquals(v4, t);
		
		t = stack.delete(n4);
		assertEquals(v4, t);
		assertEquals(2,stack.depth());
		assertEquals(3,stack.size());
		t = stack.get(n1);
		assertNull(t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertNull(t);
		t = stack.get(n4);
		assertNull(t);
		
		t = stack.pop();
		assertEquals(s2, t);
		assertEquals(1,stack.depth());
		assertEquals(2,stack.size());
		t = stack.get(n1);
		assertNull(t);
		t = stack.get(n2);
		assertEquals(v2, t);
		t = stack.get(n3);
		assertNull(t);
		t = stack.get(n4);
		assertNull(t);
		
		t = stack.delete(n2);
		assertEquals(v2, t);
		assertEquals(1,stack.depth());
		assertEquals(1,stack.size());
		t = stack.get(n1);
		assertNull(t);
		t = stack.get(n2);
		assertNull(t);
		t = stack.get(n3);
		assertNull(t);
		t = stack.get(n4);
		assertNull(t);
		
		
	}
	
	@Test
	public void basicStack(){
		RPNStack stack = new RPNStack();
		RPNObject t;
		
		assertEquals(0,stack.depth());
		assertEquals(0,stack.size());
		
		stack.push(s1);
		t = stack.peek();
		assertEquals(s1, t);
		assertEquals(1,stack.depth());
		assertEquals(1,stack.size());
		assertFalse(stack.isEmpty());
		
		
		stack.push(s2);
		t = stack.peek();
		assertEquals(s2, t);
		assertEquals(2,stack.depth());
		assertEquals(2,stack.size());
		assertFalse(stack.isEmpty());
		
		stack.push(s3);
		t = stack.peek();
		assertEquals(s3, t);
		assertEquals(3,stack.depth());
		assertEquals(3,stack.size());
		assertFalse(stack.isEmpty());
		
		t = stack.pop();
		assertEquals(s3, t);
		assertEquals(2,stack.depth());
		assertEquals(2,stack.size());
		assertFalse(stack.isEmpty());
		
		t = stack.pop();
		assertEquals(s2, t);
		assertEquals(1,stack.depth());
		assertEquals(1,stack.size());
		assertFalse(stack.isEmpty());
		
		t = stack.pop();
		assertEquals(s1, t);
		assertEquals(0,stack.depth());
		assertEquals(0,stack.size());
		assertTrue(stack.isEmpty());
	}
	

	
}
